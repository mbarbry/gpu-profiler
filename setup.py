from distutils.core import setup
import setuptools

setup(
    name='gpu_profiler',
    description='A module for monitoring gpu usage of a program',
    long_description=open('README.md').read(),
    version='0.1.1',
    author='Marc Barbry',
    author_email='marc.barbry@mailoo.org',
    url='http://pypi.python.org/pypi/memory_profiler',
    scripts=['gpuprof'],
    license='MIT'

)
