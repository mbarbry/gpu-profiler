Description
===========

Python module to monitor the GPU (nvidia) memory and clocks.
The module launch the nvidia-smi command in the same time than your program and then read the information from the
output in order to plot human readable graphics.
The actual version is based on the phylosophy of the memory_profiler from https://github.com/fabianp/memory_profiler
but apply to GPU (and without the nice python functions).

Requirements
============

To install gpuprof you need python with [matplotlib](http://matplotlib.org/) and [numpy](http://www.numpy.org/) installed.

Then you just need to download the code, go to the source directory and run,
  
    python setup.py install


To run the program,

    gpuprof run <executable>

plot the graphs,

    gpuprof plot


Some figures,

![Alt text](pictures/gpuprofile_20170203003048_utilization.jpg)
![Alt text](pictures/gpuprofile_20170203003048_memory.jpg)
